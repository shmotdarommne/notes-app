package com.company.notesapplication.repositories;

import com.company.notesapplication.models.entities.NoteHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface NoteHistoryRepository extends JpaRepository<NoteHistory, Long> {
    List<NoteHistory> findAllByNote_Id(Long noteId);
}
