package com.company.notesapplication.repositories;

import com.company.notesapplication.models.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
}
