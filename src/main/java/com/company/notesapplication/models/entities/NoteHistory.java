package com.company.notesapplication.models.entities;


import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "note_history")
public class NoteHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String label;
    private String content;
    private Date updateDate;
    private Date createDate;

    @ManyToOne
    @JoinColumn(name = "note_id")
    private Note note;
}
