package com.company.notesapplication.models.dto;

import lombok.*;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserChangingPassDto {
    private String password;
}
