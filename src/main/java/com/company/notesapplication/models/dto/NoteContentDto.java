package com.company.notesapplication.models.dto;

import lombok.*;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NoteContentDto {
    private Long id;
    private String content;
}
