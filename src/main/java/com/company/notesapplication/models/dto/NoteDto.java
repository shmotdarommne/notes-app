package com.company.notesapplication.models.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@Builder
public class NoteDto {
    private Long id;
    private String label;
    private String content;
    private Date createDate;
    private Date updateDate;
}
