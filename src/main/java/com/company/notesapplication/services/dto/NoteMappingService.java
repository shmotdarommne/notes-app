package com.company.notesapplication.services.dto;

import com.company.notesapplication.models.dto.NoteContentDto;
import com.company.notesapplication.models.dto.NoteCreateDto;
import com.company.notesapplication.models.dto.NoteDto;

import java.util.List;

public interface NoteMappingService {

    NoteDto create(Long id, NoteCreateDto noteCreateDto) throws Exception;

    NoteDto get(long id) throws Exception;

    List<NoteDto> getByUserId(Long userId) throws Exception;

    List<NoteDto> getHistory(Long noteId) throws Exception;

    NoteDto updateContent(NoteContentDto contentDto) throws Exception;

    void delete(long id) throws Exception;
}
