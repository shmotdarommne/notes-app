package com.company.notesapplication.services.dto;

import com.company.notesapplication.models.dto.UserChangingPassDto;
import com.company.notesapplication.models.dto.UserDto;

import java.util.List;

public interface UserMappingService {

    UserDto create(UserDto userDto) throws Exception;

    UserDto get(long id) throws Exception;

    List<UserDto> get();

    UserDto changePassword(long id, UserChangingPassDto userChangingPassDto) throws Exception;

    void delete(long id);

}
