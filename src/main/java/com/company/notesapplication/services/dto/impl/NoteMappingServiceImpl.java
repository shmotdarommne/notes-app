package com.company.notesapplication.services.dto.impl;

import com.company.notesapplication.models.dto.NoteContentDto;
import com.company.notesapplication.models.dto.NoteCreateDto;
import com.company.notesapplication.models.dto.NoteDto;
import com.company.notesapplication.models.entities.Note;
import com.company.notesapplication.models.entities.NoteHistory;
import com.company.notesapplication.models.entities.User;
import com.company.notesapplication.services.dto.NoteMappingService;
import com.company.notesapplication.services.main.NoteHistoryService;
import com.company.notesapplication.services.main.NoteService;
import com.company.notesapplication.services.main.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class NoteMappingServiceImpl implements NoteMappingService {

    private final NoteService noteService;
    private final UserService userService;
    private final NoteHistoryService noteHistoryService;

    private static final String ADMIN_ROLE = "ROLE_ADMIN";
    private static final String ERROR = "not enough authority";

    private Authentication a;


    @Autowired
    public NoteMappingServiceImpl(NoteService noteService, UserService userService, NoteHistoryService noteHistoryService) {
        this.noteService = noteService;
        this.userService = userService;
        this.noteHistoryService = noteHistoryService;
        this.a = SecurityContextHolder.getContext().getAuthentication();
    }

    @Override
    public NoteDto create(Long id, NoteCreateDto noteCreateDto) throws Exception {
        a = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByLogin(a.getName());

        if (a.getAuthorities().stream().noneMatch(role -> role.getAuthority().equals(ADMIN_ROLE)) &&
                !user.getId().equals(id)) {
            throw new Exception(ERROR);
        }

        Note note = noteService.create(Note.builder()
                .label(noteCreateDto.getLabel())
                .content(noteCreateDto.getContent())
                .createDate(new Date())
                .updateDate(new Date())
                .user(userService.get(id))
                .build());
        return toDto(note);
    }

    @Override
    public List<NoteDto> getByUserId(Long userId) throws Exception {
        a = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByLogin(a.getName());

        if (a.getAuthorities().stream().noneMatch(role -> role.getAuthority().equals(ADMIN_ROLE))
                && !user.getId().equals(userId)) {
            throw new Exception(ERROR);
        }
        List<Note> noteList = noteService.getByUserId(userId);
        return noteList.stream().map(this::toDto).collect(Collectors.toList());
    }

    @Override
    public List<NoteDto> getHistory(Long noteId) throws Exception {
        a = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByLogin(a.getName());

        if (a.getAuthorities().stream().noneMatch(role -> role.getAuthority().equals(ADMIN_ROLE))
                && noteService.getByUserId(user.getId()).stream().noneMatch(note -> note.getId().equals(noteId))) {
            throw new Exception(ERROR);
        }
        return noteHistoryService.getAll(noteId).stream()
                .map(this::toDtoForHistory)
                .collect(Collectors.toList());
    }

    @Override
    public NoteDto get(long id) throws Exception {
        a = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByLogin(a.getName());

        if (a.getAuthorities().stream().noneMatch(role -> role.getAuthority().equals(ADMIN_ROLE))
                && noteService.getByUserId(user.getId()).stream().noneMatch(note -> note.getId().equals(id))) {
            throw new Exception(ERROR);
        }
        Note note = noteService.get(id);
        return toDto(note);
    }

    @Override
    public NoteDto updateContent(NoteContentDto newNote) throws Exception {
        a = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByLogin(a.getName());

        if (a.getAuthorities().stream().noneMatch(role -> role.getAuthority().equals(ADMIN_ROLE))
                && noteService.getByUserId(user.getId()).stream().noneMatch(note -> note.getId().equals(newNote.getId()))) {
            throw new Exception(ERROR);
        }

        Note note = noteService.get(newNote.getId());
        noteHistoryService.create(NoteHistory.builder()
                .content(note.getContent())
                .label(note.getLabel())
                .updateDate(note.getUpdateDate())
                .createDate(note.getCreateDate())
                .note(note)
                .build());
        return toDto(noteService.update(Note.builder()
                .id(newNote.getId())
                .content(newNote.getContent())
                .build()));
    }

    @Override
    public void delete(long id) throws Exception {
        a = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByLogin(a.getName());

        if (a.getAuthorities().stream().noneMatch(role -> role.getAuthority().equals(ADMIN_ROLE))
                && noteService.getByUserId(user.getId()).stream().noneMatch(note -> note.getId().equals(id))) {
            throw new Exception(ERROR);
        }
        noteService.delete(id);
    }

    private NoteDto toDto(Note note) {
        return NoteDto.builder()
                .id(note.getId())
                .label(note.getLabel())
                .content(note.getContent())
                .createDate(note.getCreateDate())
                .updateDate(note.getUpdateDate())
                .build();
    }

    private NoteDto toDtoForHistory(NoteHistory noteHistory) {
        return NoteDto.builder()
                .id(noteHistory.getId())
                .label(noteHistory.getLabel())
                .content(noteHistory.getContent())
                .createDate(noteHistory.getCreateDate())
                .updateDate(noteHistory.getUpdateDate())
                .build();
    }
}
