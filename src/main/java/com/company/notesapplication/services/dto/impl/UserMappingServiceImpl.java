package com.company.notesapplication.services.dto.impl;

import com.company.notesapplication.models.dto.UserChangingPassDto;
import com.company.notesapplication.models.dto.UserDto;
import com.company.notesapplication.models.entities.Role;
import com.company.notesapplication.models.entities.User;
import com.company.notesapplication.services.dto.UserMappingService;
import com.company.notesapplication.services.main.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserMappingServiceImpl implements UserMappingService {

    private final UserService userService;

    private Authentication a;

    private static final String ADMIN_ROLE = "ROLE_ADMIN";
    private static final String ERROR = "not enough authority";

    @Autowired
    public UserMappingServiceImpl(UserService userService) {
        this.userService = userService;
        this.a = SecurityContextHolder.getContext().getAuthentication();
    }

    @Override
    public UserDto create(UserDto userDto) throws Exception {
        User user = userService.create(User.builder()
                .firstName(userDto.getFirstName())
                .lastName(userDto.getLastName())
                .login(userDto.getLogin())
                .password(userDto.getPassword())
                .birthday(userDto.getBirthday())
                .roles(userDto.getRoles().stream().map(role -> Role.builder()
                        .name("ROLE_" + role)
                        .build()).collect(Collectors.toSet()))
                .build());
        return toDto(user);
    }

    @Override
    public UserDto get(long id) throws Exception {
        a = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByLogin(a.getName());

        if (a.getAuthorities().stream().noneMatch(role -> role.getAuthority().equals(ADMIN_ROLE))
                && !user.getId().equals(id)) {
            throw new Exception(ERROR);
        }
        return toDto(userService.get(id));
    }

    @Override
    public UserDto changePassword(long id, UserChangingPassDto userChangingPassDto) throws Exception {
        a = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByLogin(a.getName());

        if (a.getAuthorities().stream().noneMatch(role -> role.getAuthority().equals(ADMIN_ROLE))
                && !user.getId().equals(id)) {
            throw new Exception(ERROR);
        }

        return toDto(userService.changePassword(id, User.builder()
                .password(userChangingPassDto.getPassword())
                .build()));
    }

    @Override
    public void delete(long id) {
        userService.delete(id);
    }

    @Override
    public List<UserDto> get() {
        return userService.get().stream().map(this::toDto).collect(Collectors.toList());
    }

    private UserDto toDto(User user) {
        return UserDto.builder()
                .id(user.getId())
                .login(user.getLogin())
                .password(user.getPassword())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .birthday(user.getBirthday())
                .roles(user.getRoles().stream().map(Role::getName).collect(Collectors.toSet()))
                .build();
    }
}
