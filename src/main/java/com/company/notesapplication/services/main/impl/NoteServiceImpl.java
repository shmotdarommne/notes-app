package com.company.notesapplication.services.main.impl;

import com.company.notesapplication.models.entities.Note;
import com.company.notesapplication.repositories.NoteRepository;
import com.company.notesapplication.services.main.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class NoteServiceImpl implements NoteService {

    private final NoteRepository noteRepository;

    @Autowired
    public NoteServiceImpl(NoteRepository noteRepository) {
        this.noteRepository = noteRepository;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public Note create(Note note) {
        return noteRepository.save(note);
    }

    @Override
    public List<Note> getByUserId(Long userId) {
        return noteRepository.findAllByUser_Id(userId);
    }

    @Transactional(readOnly = true)
    @Override
    public Note get(long id) {
        return noteRepository.findById(id).orElseThrow(() -> new RuntimeException("Note not found"));
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public Note update(Note newNote) {
        Note note = noteRepository.findById(newNote.getId()).orElseThrow(() -> new RuntimeException("Note not found"));
        note.setContent(newNote.getContent());
        noteRepository.save(note);
        return note;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)

    @Override
    public void delete(long id) {
        noteRepository.deleteById(id);
    }
}
