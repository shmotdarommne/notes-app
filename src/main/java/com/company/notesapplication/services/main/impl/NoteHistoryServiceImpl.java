package com.company.notesapplication.services.main.impl;

import com.company.notesapplication.models.entities.NoteHistory;
import com.company.notesapplication.repositories.NoteHistoryRepository;
import com.company.notesapplication.services.main.NoteHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NoteHistoryServiceImpl implements NoteHistoryService {

    private final NoteHistoryRepository noteHistoryRepository;

    @Autowired
    public NoteHistoryServiceImpl(NoteHistoryRepository noteHistoryRepository) {
        this.noteHistoryRepository = noteHistoryRepository;
    }

    @Override
    public NoteHistory create(NoteHistory noteHistory) {
        return noteHistoryRepository.save(noteHistory);
    }

    @Override
    public List<NoteHistory> getAll(Long noteId) {
        return noteHistoryRepository.findAllByNote_Id(noteId);
    }
}
