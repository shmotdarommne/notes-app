package com.company.notesapplication.services.main;

import com.company.notesapplication.models.entities.NoteHistory;

import java.util.List;

public interface NoteHistoryService {

    NoteHistory create(NoteHistory noteHistory);

    List<NoteHistory> getAll(Long noteId);
}
