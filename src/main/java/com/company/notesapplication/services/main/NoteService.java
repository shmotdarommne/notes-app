package com.company.notesapplication.services.main;

import com.company.notesapplication.models.entities.Note;

import java.util.List;

public interface NoteService {
    Note create(Note note);

    Note get(long id);

    List<Note> getByUserId(Long userId);

    Note update(Note newNote);

    void delete(long id);
}
