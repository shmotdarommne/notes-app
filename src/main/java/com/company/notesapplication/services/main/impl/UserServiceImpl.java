package com.company.notesapplication.services.main.impl;

import com.company.notesapplication.models.entities.Role;
import com.company.notesapplication.models.entities.User;
import com.company.notesapplication.repositories.RoleRepository;
import com.company.notesapplication.repositories.UserRepository;
import com.company.notesapplication.services.main.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserDetailsService, UserService {

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository) {
        Role role1 = roleRepository.save(Role.builder()
                .name("ROLE_USER")
                .build());
        Role role2 = roleRepository.save(Role.builder()
                .name("ROLE_ADMIN")
                .build());

        userRepository.save(User.builder()
                .login("user")
                .password("$2y$12$ttsAVnjqgeU.pq.84snMQONwvFwAPAv.TaDafhPKo17pBA9fIrnmi")
                .firstName("marat")
                .lastName("yep")
                .birthday(new Date())
                .roles(Set.of(role1))
                .build());
        userRepository.save(User.builder()
                .login("admin")
                .password("$2y$12$ttsAVnjqgeU.pq.84snMQONwvFwAPAv.TaDafhPKo17pBA9fIrnmi")
                .firstName("sasha")
                .lastName("yep")
                .birthday(new Date())
                .roles(Set.of(role2))
                .build());
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public User create(User user) throws Exception {
        User userOld = userRepository.findByLogin(user.getLogin());
        if (userOld != null) {
            throw new Exception("Duplicate user");
        }
        roleRepository.saveAll(user.getRoles());
        return userRepository.save(user);
    }

    @Transactional(readOnly = true)
    @Override
    public List<User> get() {
        return userRepository.findAll();
    }


    @Transactional(readOnly = true)
    @Override
    public User get(long id) {
        return userRepository.findById(id).orElseThrow(() -> new RuntimeException("User not found"));
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public User changePassword(long id, User newUser) {
        User user = userRepository.findById(id).orElseThrow(() -> new RuntimeException("User not found"));
        user.setPassword(newUser.getPassword());
        userRepository.save(user);
        return user;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public User findByLogin(String login) {
        return userRepository.findByLogin(login);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public void delete(long id) {
        userRepository.deleteById(id);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {

        User user = userRepository.findByLogin(login);
        if (user == null) {
            throw new UsernameNotFoundException(String.format("User '%s' not found", login));
        }

        return new org.springframework.security.core.userdetails.User(user.getLogin(), user.getPassword(), mapRolesToAuth(user.getRoles()));

    }

    private Collection<? extends GrantedAuthority> mapRolesToAuth(Collection<Role> roles) {
        return roles.stream().map(role -> new SimpleGrantedAuthority(role.getName())).collect(Collectors.toList());
    }
}
