package com.company.notesapplication.services.main;

import com.company.notesapplication.models.entities.User;

import java.util.List;

public interface UserService {
    User create(User user) throws Exception;

    User findByLogin(String login);

    List<User> get();

    User get(long id);

    User changePassword(long id, User user);

    void delete(long id);
}
