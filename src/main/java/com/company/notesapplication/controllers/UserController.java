package com.company.notesapplication.controllers;


import com.company.notesapplication.models.dto.UserChangingPassDto;
import com.company.notesapplication.models.dto.UserDto;
import com.company.notesapplication.services.dto.UserMappingService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
@AllArgsConstructor
public class UserController {

    private final UserMappingService userMappingService;

    @PostMapping("/admin/")
    public UserDto create(@RequestBody UserDto userDto) throws Exception {
        return userMappingService.create(userDto);
    }

    @GetMapping("/{id}")
    public UserDto get(@PathVariable Long id) throws Exception {
        return userMappingService.get(id);
    }

    @GetMapping("/admin/all")
    public List<UserDto> get() {
        return userMappingService.get();
    }

    @PutMapping("/{id}")
    public UserDto updatePass(@PathVariable Long id, @RequestBody UserChangingPassDto userChangingPassDto) throws Exception {
        return userMappingService.changePassword(id, userChangingPassDto);
    }

    @DeleteMapping("/admin/{id}")
    public void delete(@PathVariable Long id) {
        userMappingService.delete(id);
    }


}
