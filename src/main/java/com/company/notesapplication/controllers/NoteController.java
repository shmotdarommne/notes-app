package com.company.notesapplication.controllers;


import com.company.notesapplication.models.dto.NoteContentDto;
import com.company.notesapplication.models.dto.NoteCreateDto;
import com.company.notesapplication.models.dto.NoteDto;
import com.company.notesapplication.services.dto.NoteMappingService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/note")
@AllArgsConstructor
public class NoteController {

    private final NoteMappingService noteMappingService;

    @PostMapping()
    public NoteDto create(@RequestParam Long userId, @RequestBody NoteCreateDto noteCreateDto) throws Exception {
        return noteMappingService.create(userId, noteCreateDto);
    }

    @GetMapping("/{id}")
    public NoteDto get(@PathVariable Long id) throws Exception {
        return noteMappingService.get(id);
    }

    @GetMapping("/all/{userId}")
    public List<NoteDto> getAll(@PathVariable Long userId) throws Exception {
        return noteMappingService.getByUserId(userId);
    }

    @PutMapping()
    public NoteDto update(@RequestBody NoteContentDto noteContentDto) throws Exception {
        return noteMappingService.updateContent(noteContentDto);
    }

    @GetMapping("/history/{noteId}")
    public List<NoteDto> getHistory(@PathVariable Long noteId) throws Exception {
        return noteMappingService.getHistory(noteId);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) throws Exception {
        noteMappingService.delete(id);
    }


}
